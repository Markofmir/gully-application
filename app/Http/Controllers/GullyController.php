<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Gully;
use App\Models\Council;
use App\Models\Assignment;
use App\Models\Service;
use App\Models\Status;
use App\Models\Note;
use App\Models\Media;
use App\Models\Information;
use App\Models\Frequency;
use App\Jobs\GulliesCsvProcess;
use App\Jobs\GulliesLocation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Mail\AssignmentEmail;
use App\Models\Comment;


class GullyController extends Controller
{
    public function index($council){
		$gullies = Gully::where('SECTION_OFFICE_NAME', $council)->paginate(10);

		return view('gullies', compact('gullies'));
	}

	public function indexGully($gully){
		$info = Gully::where('id', $gully)->get();
		$comments = Comment::where('gully_id', $gully)->get();
		return view('gully', compact('info', 'comments'));
	}

    public function uploadFile(){
    	return view ('upload-file');
    }

    public function upload(){
    	if(request()->has('mycsv')){
			$data = file(request()->mycsv);

			$chunks = array_chunk($data, 1000);

			$header = [];

			foreach ($chunks as $key => $chunk) {
	        	
	        	$data = array_map('str_getcsv', $chunk);

	            if($key == 0){
	               	$header = $data[0];
	               	unset($data[0]);
	            }

	        	GulliesCsvProcess::dispatch($data, $header);
	        }

			return 'Stored';

		}

		
		return 'please upload file';
	}

	public function returnGullyLocations(){
		// return Gully::whereIn('SECTION_OFFICE_NAME', ['ANTRIM & NEWTOWNABBEY'])->get();
		return Gully::whereIn('id', [43368])->get();
	}

	public function addGully(Request $request){
    	$gully = new Gully;
    	$gully->SECTION_NAME = $request->SECTION_NAME;
    	$gully->SECTION_OFFICE_NAME = $request->SECTION_OFFICE_NAME;
    	$gully->GULLY_TYPE_NAME = $request->GULLY_TYPE_NAME;
    	$gully->START_METRES = $request->START_METRES;
    	$gully->END_METRES = $request->END_METRES;
    	$gully->EASTING = $request->EASTING;
		$gully->NORTHING = $request->NORTHING;
		$gully->LOCATION = $request->LOCATION;
    	$gully->save();

    	return redirect('/');
    }

    public function assignGully(Request $request){
    	// Gully::where('id', [$request->gully_id])->update(array('contractor_id' => $request->contractor_id));
		// return redirect('/');
		
		$assignment = new Assignment;
		$assignment->gully_id = $request->gully_id;
		$assignment->assignee_id = $request->assignee_id;
		$assignment->assigner_id = $request->assigner_id;
		$assignment->priority = $request->priority;
		$assignment->save();

		if($request->filled('notes')){
			$new_assignment_id = DB::table('assignments')->orderBy('id', 'desc')->first();

			$note = new Note;
			$note->assignment_id = $new_assignment_id->id;
			$note->notes = $request->notes;
			$note->save();

			$user = User::where('id', '=', $request->assignee_id)->get();
			\Mail::to($user->first()->email)->send(new AssignmentEmail($assignment, $note));
		} else {
			$note = new Note;
			$note->notes = "No note specified.";
			$user = User::where('id', '=', $request->assignee_id)->get();
			\Mail::to($user->first()->email)->send(new AssignmentEmail($assignment, $note));
		}
		return redirect('/admin');
	}
	
	public function getMyGullies(){
		// ['gullies.id', '=', 'assignments.gully_id'],
		// ['assignments.assignee_id', '=', Auth::id()]

		$gullies = Gully::whereHas('assignments', function ($query){
			return $query->where('assignee_id', '=', Auth::id());
		})->get();

		return view('mygullies', compact('gullies'));
	}

    public function sendGullyReport(){
		return view('sendgullyreport');
	}

	public function sendReport(Request $request){
		$service = new Service;
		$service->gully_id = $request->gully_id;
		$service->contractor_id = $request->contractor_id;
		$service->gully_state = $request->gully_state;
		$service->save();

		if($request->filled('notes')){
			$new_service_id = DB::table('services')->orderBy('id', 'desc')->first();

			$note = new Note;
			$note->service_id = $new_service_id->id;
			$note->notes = $request->notes;
			$note->save();
		}

		if($request->hasFile('file')){
			$new_service_id = DB::table('services')->orderBy('id', 'desc')->first();

			$request->validate(['image' => 'mimes:jpeg,bmp,png']);

			$request->file->store('media', 'public');

			$media = new Media([
				"gully_id" => $request->gully_id,
				"service_id" => $new_service_id->id,
				"file_path" => $request->file->hashName()
			]);
			$media->save();
		}

		if(Status::where('gully_id', '=', $request->gully_id)->exists()) {
			// This code is usually for a single record
			// The find() needs something in the brackets
			// $existing_status = Status::find();
			// $existing_status->gully_state = $request->gully_state;
			// $existing_status->save();

			// This code is used for mass updates, but works for me as I
			// have a really stringent where clause
			Status::where('gully_id', '=', $request->gully_id)->update(['gully_state' => $request->gully_state]);
			
		} else {
			$new_status = new Status;
			$new_status->gully_id = $request->gully_id;
			$new_status->gully_state = $request->gully_state;
			$new_status->save();
		}
			
		//TODO:  add 'with' message to say we sent the report.
		return redirect('/my_gullies');
	}

	public function viewFiles($gully){
		$images = Media::where('gully_id', '=', $gully)->get();

		return view('seefiles', compact('images'));
	}

	public function indexAdmin(){
		$today_reports = Service::whereDate('created_at', '=', Carbon::today())->get();
		$all_reports = Service::all();
		$today_assignments = Assignment::whereDate('created_at', '=', Carbon::today())->get();
		$all_assignments = Assignment::all();

		return view('admin', compact('today_reports', 'all_reports', 'today_assignments', 'all_assignments'));
	}

	public function showAddInformation(){
		return view('addinformation');
	}

	public function addInformation(Request $request){
		$information = new Information;
		$information->gully_id = $request->gully_id;
		$information->information = $request->information;
		$information->save();

		return redirect('/my_gullies');
	}

	public function viewInformation($gully){
		$information = Information::where('gully_id', '=', $gully)->get();

		return view('viewinformation', compact('information'));
	}

	public function changeFrequency(Request $request){
		if(Frequency::where('gully_id', '=', $request->gully_id)->exists()) {
			// This code is usually for a single record
			// The find() needs something in the brackets
			// $existing_status = Status::find();
			// $existing_status->gully_state = $request->gully_state;
			// $existing_status->save();

			// This code is used for mass updates, but works for me as I
			// have a really stringent where clause
			Frequency::where('gully_id', '=', $request->gully_id)->update(['frequency' => $request->frequency]);
			
		} else {
			$new_frequency = new Frequency;
			$new_frequency->gully_id = $request->gully_id;
			$new_frequency->frequency = $request->frequency;
			$new_frequency->save();
		}

		return redirect('/admin');
	}

	public function showAddComment($gully){
		$info = Gully::where('id', $gully)->get();

		return view('addcomment', compact('info'));
	}

	public function addComment(Request $request){
		$new_comment = new Comment;
		$new_comment->gully_id = $request->gully_id;
		$new_comment->comment = $request->comment;
		$new_comment->save();

		return redirect('/');
	}
}
