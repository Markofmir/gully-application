<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class APIController extends Controller
{
    function getUsersWithRoles(){
        $count_users = DB::table('users')->select(DB::raw('count(*) as user_count'))->get();

        $users_with_role = DB::table('model_has_roles')
        ->select(DB::raw('count(*) as user_count, role_id'))
        ->groupBy('role_id')
        ->get();

        // Arr::collapse can be used to make multiple arrays into one array
        // Look it up in the official laravel documentation
        return ['number_of_users' => $count_users, 'users_with_role' => $users_with_role];
    }

    function getNumberOfReports(){
        $number_of_reports = DB::table('services')->select(DB::raw('count(*) as report_count'))
        ->get();
        return['number_of_reports' => $number_of_reports];
    }

    function getWeeklyReports(){
        // Variables for date
        $six_days_ago = strtotime("-6 Days");
        $five_days_ago = strtotime("-5 Days");
        $four_days_ago = strtotime("-4 Days");
        $three_days_ago = strtotime("-3 Days");
        $two_days_ago = strtotime("-2 Days");
        $yesterday = strtotime("yesterday");

        // This gets reports made 6 days ago
        $reports_six_days_ago = DB::table('services')
        ->select(DB::raw('count(*) as report_count'))
        ->where('created_at', '>', date("Y-m-d", $six_days_ago))
        ->where('created_at', '<', date("Y-m-d", $five_days_ago))
        ->get();

        // This gets reports made 5 days ago
        $reports_five_days_ago = DB::table('services')
        ->select(DB::raw('count(*) as report_count'))
        ->where('created_at', '>', date("Y-m-d", $five_days_ago))
        ->where('created_at', '<', date("Y-m-d", $four_days_ago))
        ->get();

        // This gets reports made 4 days ago
        $reports_four_days_ago = DB::table('services')
        ->select(DB::raw('count(*) as report_count'))
        ->where('created_at', '>', date("Y-m-d", $four_days_ago))
        ->where('created_at', '<', date("Y-m-d", $three_days_ago))
        ->get();

        // This gets reports made 3 days ago
        $reports_three_days_ago = DB::table('services')
        ->select(DB::raw('count(*) as report_count'))
        ->where('created_at', '>', date("Y-m-d", $three_days_ago))
        ->where('created_at', '<', date("Y-m-d", $two_days_ago))
        ->get();

        // This gets reports made 2 days ago
        $reports_two_days_ago = DB::table('services')
        ->select(DB::raw('count(*) as report_count'))
        ->where('created_at', '>', date("Y-m-d", $two_days_ago))
        ->where('created_at', '<', date("Y-m-d", $yesterday))
        ->get();

        // This gets reports made yesterday
        $reports_yesterday = DB::table('services')
        ->select(DB::raw('count(*) as report_count'))
        ->where('created_at', '>', date("Y-m-d", $yesterday))
        ->where('created_at', '<', date("Y-m-d"))
        ->get();

        // This gets reports made today
        $reports_today = DB::table('services')
        ->select(DB::raw('count(*) as report_count'))
        ->where('created_at', '>', date("Y-m-d"))
        ->get();

        // This gets reports made this week
        $reports_whole_week = DB::table('services')
        ->select(DB::raw('count(*) as report_count'))
        ->where('created_at', '>', date("Y-m-d", $six_days_ago))
        ->get();

        // Return each day's caulcualted array
        return ['weekly_reports' => [
            'reports_whole_week' => $reports_whole_week,
            'reports_today' => $reports_today, 
            'reports_yesterday' => $reports_yesterday, 
            'reports_two_days_ago' => $reports_two_days_ago, 
            'reports_three_days_ago' => $reports_three_days_ago, 
            'reports_four_days_ago' => $reports_four_days_ago, 
            'reports_five_days_ago' => $reports_five_days_ago, 
            'reports_six_days_ago' => $reports_six_days_ago
            ]
        ];
    }
}
