<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Records;
use App\Models\Council;


class CouncilController extends Controller
{	
	public function index(){
		$councils = Council::all();
		// return view ('welcome');
		return view('welcome', compact('councils'));
	}

    public function addCouncil(Request $request){
    	$council = new Council();
    	$council->council_name = $request->council_name;
    	$council->save();

    	return redirect('/');
    }
}
