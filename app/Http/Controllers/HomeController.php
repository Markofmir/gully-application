<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        //  /\/\/\/\/\/\/\/\
        // < CODE GRAVEYARD >
        //  \/\/\/\/\/\/\/\/
        // Contains old code and useful bits if needed in future
        //
        // $role = Role::findById(2);
        // $permission = Permission::findById(3);
        // $role->givePermissionTo($permission);
        // auth()->user()->givePermissionTo('send cleaning report');
        // auth()->user()->assignRole('admin');
        // return auth()->user()->permissions;
        // auth()->user()->revokePermissionTo('send cleaning report');
        // auth()->user()->removeRole('contractor');

        // // Create neccessary roles
        // Role::create(['name'=>'admin']);
        // Role::create(['name'=>'contractor']);
        // Role::create(['name'=>'technician']);
        // Role::Create(['name' => 'repairer']);
        // Role::Create(['name' => 'gully-cleaner']);

        // // Create neccessary permissions
        // Permission::create(['name'=>'send cleaning report']);
        // Permission::create(['name'=>'assign gully']);
        // Permission::Create(['name'=>'add gully']);
        // Permission::Create(['name'=>'repair gully']);
        // Permission::Create(['name'=>'clean gully']);

        // // Make a variable for each role to allow assigning of permissions
        // $admin = Role::findById(1);
        // $contractor = Role::findById(2);
        // $technician = Role::findById(3);
        // $repairer = Role::findById(4);
        // $cleaner = Role::findById(5);

        // // Make a variable for each permission to allow assignment to roles
        // $report = Permission::findById(1);
        // $assign = Permission::findById(2);
        // $add = Permission::findById(3);
        // $repair = Permission::findById(4);
        // $clean = Permission::findById(5);
        
        // // Assign all permissions to admins
        // $admin->givePermissionTo($report);
        // $admin->givePermissionTo($assign);
        // $admin->givePermissionTo($add);
        // $admin->givePermissionTo($repair);
        // $admin->givePermissionTo($clean);
        
        // // Assign all NECESSARY permissions to contractors
        // $contractor->givePermissionTo($report);
        // $contractor->givePermissionTo($clean);

        // // Assign all NECESSARY permissions to technicians
        // $technician->givePermissionTo($assign);
        // $technician->givePermissionTo($add);

        // // Assign all NECESSARY permissions to engineers
        // $repairer->givePermissionTo($repair);

        // // Assign all NECESSARY permission to cleaners
        // $cleaner->givePermissionTo($clean);
        // $cleaner->givePermissionTo($report);

        // // Run once to initialise first admin (Mark)
        // auth()->user()->assignRole('admin');
        
        // Return the home view because that is what this function is ACTUALLY for
        return view('home');
    }
}
