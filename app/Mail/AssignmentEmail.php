<?php

namespace App\Mail;

use App\Models\Assignment;
use App\Models\Note;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AssignmentEmail extends Mailable
{   
    use Queueable, SerializesModels;

    public $assignment, $note;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Assignment $assignment, Note $note)
    {
        //
        $this->assignment = $assignment;
        $this->note = $note;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.assignment');
    }
}
