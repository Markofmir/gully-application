<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    public function gully(){
    	return $this->belongsTo('App\Models\Gully', 'gully_id', 'id');
    }

    public function assignee(){
    	return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function statuses(){
    	return $this->hasOne('App\Models\Status', 'gully_state', 'gully_state');
    }

    public function notes(){
    	return $this->hasOne('App\Models\Note', 'service_id', 'id');
    }
}
