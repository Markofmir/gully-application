<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Council extends Model
{
    use HasFactory;

    protected $table = "councils";

    public function gullies(){
    	return $this->hasMany(Gully::class);
    }

}
