<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    use HasFactory;

    protected $table = 'assignments';

    public function gully(){
    	return $this->belongsTo('App\Models\Gully', 'gully_id', 'id');
    }

    public function assignee(){
    	return $this->belongsTo('App\Models\User', 'assignee_id', 'id');
    }

    public function assigner(){
    	return $this->belongsTo('App\Models\User', 'assigner_id', 'id');
    }

    public function notes(){
    	return $this->hasOne('App\Models\Note', 'assignment_id', 'id');
    }
}
