<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HasFactory;

    public function gully(){
    	return $this->belongsTo('App\Models\Gully', 'gully_id', 'id');
    }

    public function services(){
    	return $this->belongsTo('App\Models\Service', 'gully_state', 'gully_state');
    }
}
