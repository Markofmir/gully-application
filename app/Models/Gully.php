<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use MStaack\LaravelPostgis\Eloquent\PostgisTrait;
use MStaack\LaravelPostgis\Geometries\Point;

class Gully extends Model
{
    use HasFactory, PostgisTrait;

    protected $guarded = [];

    protected $table = "gullies";

    protected $postgisFields = [
        'LOCATION',
    ];

    protected $postgisTypes = [
        'LOCATION' => [
            'geomtype' => 'geometry',
            'srid' => 27700
        ]
    ];

    public function gully(){
    	return $this->belongsTo(Council::class);
    }

    public function assignments(){
    	return $this->hasMany('App\Models\Assignment', 'gully_id', 'id');
    }

    public function services(){
    	return $this->hasMany('App\Models\Service', 'gully_id', 'id');
    }

    public function statuses(){
    	return $this->hasOne('App\Models\Status', 'gully_id', 'id');
    }

    public function information(){
    	return $this->hasMany('App\Models\Information', 'gully_id', 'id');
    }

    public function frequencies(){
    	return $this->hasOne('App\Models\Frequency', 'gully_id', 'id');
    }
}
