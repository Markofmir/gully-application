<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    use HasFactory;

    public function assignments(){
    	return $this->belongsTo('App\Models\Assignment', 'assignment_id', 'id');
    }

    public function services(){
    	return $this->belongsTo('App\Models\Service', 'service_id', 'id');
    }
}
