<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGulliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gullies', function (Blueprint $table) {
            $table->id();
            $table->string('SECTION_NAME');
            $table->string('SECTION_OFFICE_NAME');
            $table->string('GULLY_TYPE_NAME');
            $table->string('START_METRES');
            $table->string('END_METRES');
            $table->string('EASTING');
            $table->string('NORTHING');
            $table->point('LOCATION', 'GEOMETRY', 27700);
            $table->timestamps();
            $table->foreign('SECTION_OFFICE_NAME')->references('council_name')->on('councils')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gullies');
    }
}