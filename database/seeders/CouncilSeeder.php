<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Models\Council;

class CouncilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['council_name' => 'ANTRIM & NEWTOWNABBEY'],
            ['council_name' => 'ARDS'],
            ['council_name' => 'ARMAGH'],
            ['council_name' => 'BANBRIDGE / CRAIGAVON'],
            ['council_name' => 'BELFAST NORTH'],
            ['council_name' => 'BELFAST SOUTH'],
            ['council_name' => 'CASTLEREAGH / LISBURN'],
            ['council_name' => 'CAUSEWAY COAST & GLENS - EAST'],
            ['council_name' => 'CAUSEWAY COAST & GLENS - WEST'],
            ['council_name' => 'COOKSTOWN / MAGHERAFELT'],
            ['council_name' => 'DOWN'],
            ['council_name' => 'DUNGANNON'],
            ['council_name' => 'FERMANAGH'],
            ['council_name' => 'LONDONDERRY'],
            ['council_name' => 'MID & EAST ANTRIM'],
            ['council_name' => 'NEWRY and MOURNE'],
            ['council_name' => 'NORTH DOWN'],
            ['council_name' => 'OMAGH'],
            ['council_name' => 'STRABANE'],
        ];

        foreach($data as $council){
            Council::create($council);
        }
    }
}
