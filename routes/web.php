<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GullyController;
use App\Http\Controllers\CouncilController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [CouncilController::class, 'index']);

Route::get('/my_gullies', [GullyController::class, 'getMyGullies']);

Route::get('/send_report/{gully}', [GullyController::class, 'sendGullyReport']);

Route::get('/view_files/{gully}', [GullyController::class, 'viewFiles']);

Route::post('/send_gully_report', [GullyController::class, 'sendReport']);

Route::get('/gullies/{council}', 'App\Http\Controllers\GullyController@index');

Route::get('/gully/{gully}', 'App\Http\Controllers\GullyController@indexGully');

Route::post('/upload', [GullyController::class, 'upload'])->middleware('role:admin');

Route::post('/add-council', [CouncilController::class, 'addCouncil'])->middleware('role:admin');

Route::post('/add-gully', [GullyController::class, 'addGully'])->middleware('role:admin');

Route::post('/assign-gully', [GullyController::class, 'assignGully'])->middleware('role:admin');

Route::post('/change-frequency', [GullyController::class, 'changeFrequency'])->middleware('role:admin');

Route::get('/add_information/{gully}', [GullyController::class, 'showAddInformation']);

Route::post('/add-information', [GullyController::class, 'addInformation']);

Route::get('view_information/{gully}', [GullyController::class, 'viewInformation']);

Route::get('/app', function () {
	return view('app');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/admin', [GullyController::class, 'indexAdmin']);

Route::get('/gully/{gully}/add-comment', 'App\Http\Controllers\GullyController@showAddComment');

Route::post('/add-comment', [GullyController::class, 'addComment']);
