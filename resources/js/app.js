/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps';


Vue.use(VueGoogleMaps, {

	load: {
		key: '',
	},

});

const app = new Vue({
    el: '#app',
    data(){
    	return {
    		gullies: [],
    		infoWindowOptions: {
    			pixelOffset: {
    				width: 0,
    				height: -35
    			}
    		},
    		activeGully: {},
    		infoWindowOpened: false
    	}
    },
    created(){
    	axios.get('/api/gullies')
    		.then((response) => this.gullies = response.data)
    		.catch((error) => console.error(error));
    },
    methods: {

		// TODO: Figure out how to get lat/lng from LOCATION
    	getPosition(r) {
			var location = JSON.parse(r.LOCATION);


    		return {
				lat: JSON.parse(location.coordinates[0]),
				lng: JSON.parse(location.coordinates[1])
			}
    	},
    	handleMarkerClicked(r){
    		this.activeGully = r;
    		this.infoWindowOpened = true;
    	},
    	handleInfoWindowClose(){
    		this.activeGully = {};
    		this.infoWindowOpened = false;
    	},
    	//handleMapClick(e) {
    	//	this.locations.push({
    	//		name: "Placeholder",
    	//		hours: "00:00am-00:00pm",
    	//		city: "Orlando",
    	//		latitude: e.latLng.lat(),
    	//		longitude: e.latLng.lng()
    	//	});
    	//}
    },
    computed: {
    	mapCenter() {
    		// if (!this.gullies.length){
    		// 	return {
    		// 		lat: 10,
    		// 		lng: 10,
    		// 	}
    		// }

			// return parseFloat(this.gullies[0].LOCATION)
			return {
				lat: 10,
				lng: 10,
			}
    	},
    	infoWindowPosition() {
    		// return {
    		// 	lat: parseFloat(this.activeGully.LATITUDE),
    		// 	lng: parseFloat(this.activeGully.LONGITUDE),
			// };
			return {
				lat: 10,
				lng: 10,
			};

    	},
    }
    

});
