<!DOCTYPE html>
<html>
<head>
	<title>Add Comment</title>

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
    <h1 class="w3-green" style="text-shadow:1px 1px 0 #444">Add Comment</h1>

    <a href="/" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-green"><b>Back</b></a>


    <form action="/add-comment" method="post">
        @csrf
        @foreach($info as $gully)
        <input type="hidden" id="gully_id" name="gully_id" value="{{$gully->id}}">
        <input type="text" name="comment" id="comment" placeholder="Comment">
        <br />
        @endforeach
        <input type="submit" value="Add Comment">
    </form>
    

</body>
</html>