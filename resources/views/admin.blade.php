<!DOCTYPE html>
<html>
    <head>
        <title>Gully Uploader</title>
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    </head>
    <body>
        <h1 class="w3-green" style="text-shadow:1px 1px 0 #444">Gully Uploader</h1>
        <div id="nav">
            <a href="/" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-green">Home</a>
            @can('send cleaning report')
            <a href="my_gullies" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-green">My Gullies</a>
            @endcan
            @role('admin')
            <a href="admin" class="w3-text-green w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-green">Admin</a>
            @endrole
        </div>
        @role('admin')
            <!-- <div id="upload csv file">
                <form action="/upload" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="file" name="mycsv" id="mycsv">
                    <input type="submit" value="Upload">
                </form> 
            </div> -->
            <!-- <div id="add council">
                <br />
                <form action="/add-council" method="post">
                    @csrf
                    <input type="text" name="council_name" id="council_name" placeholder="Add Council...">
                    <input type="submit" value="Add Council">
                </form>
            </div> -->
            <div id="add gully">
                <br />
                <form action="/add-gully" method="post">
                    @csrf
                    <input type="text" name="SECTION_NAME" id="SECTION_NAME" placeholder="Section Name">
                    <br />
                    <input type="text" name="SECTION_OFFICE_NAME" id="SECTION_OFFICE_NAME" placeholder="Section Office Name">
                    <br />
                    <input type="text" name="GULLY_TYPE_NAME" id="GULLY_TYPE_NAME" placeholder="Gully Type Name">
                    <br />
                    <input type="text" name="START_METRES" id="START_METRES" placeholder="Start Metres">
                    <br />
                    <input type="text" name="END_METRES" id="END_METRES" placeholder="End Metres">
                    <br />
                    <input type="text" name="EASTING" id="EASTING" placeholder="Easting">
                    <br />
                    <input type="text" name="NORTHING" id="NORTHING" placeholder="Northing">
                    <br />
                    <input type="text" name="LOCATION" id="LOCATION" placeholder="Location">
                    <br />
                    <input type="submit" value="Add Gully">
                </form>
            </div>
            <div id="assign gully">
                <br />
                <form action="/assign-gully" method="post">
                    @csrf
                    <input type="text" name="gully_id" id="gully_id" placeholder="Gully ID">
                    <br />
                    <input type="text" name="assignee_id" id="assignee_id" placeholder="Assignee ID">
                    <br />
                    <input type="text" name="assigner_id" id="assigner_id" placeholder="Assigner ID">
                    <br />
                    <input type="text" name="priority" id="priority" placeholder="Priority">
                    <br />
                    <input type="text" name="notes" id="notes" placeholder="Notes...">
                    <br />
                    <input type="submit" value="Assign Gully">
                </form>
            </div>
            <div id="change frequency">
                <br />
                <form action="/change-frequency" method="post">
                    @csrf
                    <input type="text" name="gully_id" id="gully_id" placeholder="Gully ID">
                    <br />
                    <input type="text" name="frequency" id="frequency" placeholder="Frequency">
                    <br />
                    <input type="submit" value="Assign Gully">
                </form>
            </div>
            <div id="today's reports">
                </br>
                <h2>Today's Reports</h2>
                <table class="w3-table w3-striped w3-border">
                    <thead>
                        <tr>
                            <td><b>Gully ID</b></td>
                            <td><b>Contractor ID</b></td>
                            <td><b>Gully State</b></td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($today_reports as $report)
                        <tr>
                            <td>{{$report->gully_id}}</td>
                            <td>{{$report->contractor_id}}</td>
                            <td>{{$report->gully_state}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div id="report history">
                </br>
                <h2>Report History</h2>
                <table class="w3-table w3-striped w3-border">
                    <thead>
                        <tr>
                            <td><b>Gully ID</b></td>
                            <td><b>Contractor ID</b></td>
                            <td><b>Gully State</b></td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($all_reports as $a_report)
                        <tr>
                            <td>{{$a_report->gully_id}}</td>
                            <td>{{$a_report->contractor_id}}</td>
                            <td>{{$a_report->gully_state}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div id="today's assignments">
                </br>
                <h2>Today's Assignments</h2>
                <table class="w3-table w3-striped w3-border">
                    <thead>
                        <tr>
                            <td><b>Assignee ID</b></td>
                            <td><b>Assigner ID</b></td>
                            <td><b>Gully ID</b></td>
                            <td><b>Priority</b></td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($today_assignments as $assignment)
                        <tr>
                            <td>{{$assignment->assignee_id}}</td>
                            <td>{{$assignment->assigner_id}}</td>
                            <td>{{$assignment->gully_id}}</td>
                            <td>{{$assignment->priority}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div id="assignment history">
                </br>
                <h2>Assignment History</h2>
                <table class="w3-table w3-striped w3-border">
                    <thead>
                        <tr>
                            <td><b>Assignee ID</b></td>
                            <td><b>Assigner ID</b></td>
                            <td><b>Gully ID</b></td>
                            <td><b>Priority</b></td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($all_assignments as $an_assignment)
                        <tr>
                            <td>{{$an_assignment->assignee_id}}</td>
                            <td>{{$an_assignment->assigner_id}}</td>
                            <td>{{$an_assignment->gully_id}}</td>
                            <td>{{$an_assignment->priority}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @endrole
    </body>
</html>