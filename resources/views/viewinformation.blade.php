<!DOCTYPE html>
<html>
<head>
	<title>Council Gullies</title>

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
    <h1 class="w3-green" style="text-shadow:1px 1px 0 #444">Send Gully Report</h1>

    <a href="/my_gullies" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-green"><b>Back</b></a>


    <h2 style="text-shadow:1px 1px 0 #444">Gully Info</h2>
        <table class="w3-table w3-striped w3-border">
            <thead>
                <tr>
                    <td><b>Information</b></td>
                </tr>
            </thead>
            <tbody>
                @foreach($information as $info)
                <tr>
                    <td>{{$info->information}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    

</body>
</html>