<!DOCTYPE html>
<html>
<head>
	<title>User Gullies</title>

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
	<div>

		<h1 class="w3-green" style="text-shadow:1px 1px 0 #444">My Gullies</h1>

		<a href="/" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-green"><b>Back</b></a>

		<a href="/app" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-green"><b>Maps</b></a>


		@can('send cleaning report')

        <h2 style="text-shadow:1px 1px 0 #444">My Gullies</h2>
        <table class="w3-table w3-striped w3-border">
            <thead>
                <tr>
                    <td><b>ID</b></td>
                    <td><b>Name</b></td>
                    <td><b>Section</b></td>
                    <td><b>Type</b></td>
                    <td><b>Location</b></td>
                    <td colspan="4"><b>Utilities</b></td>
                </tr>
            </thead>
            <tbody>
                @foreach($gullies as $gully)
                <tr>
                    <td>{{$gully->id}}</td>
                    <td>{{$gully->SECTION_NAME}}</td>
                    <td>{{$gully->SECTION_OFFICE_NAME}}</td>
                    <td>{{$gully->GULLY_TYPE_NAME}}</td>
                    <td>{{$gully->LOCATION}}</td>
                    <td>
                        <form  method="post">
                            @csrf
                            <a href="{{ url('send_report/'.$gully->SECTION_NAME) }}" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-hover-border-green">Send Report</a>
                        </form>
                    </td>
                    <td>
                        <form>
                            @csrf
                            <a href="{{ url('view_files/'.$gully->id) }}" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-hover-border-green">View Files</a>
                        </form>
                    </td>
                    <td>
                        <form  method="post">
                            @csrf
                            <a href="{{ url('add_information/'.$gully->id) }}" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-hover-border-green">Add Information</a>
                        </form>
                    </td>
                    <td>
                        <form  method="post">
                            @csrf
                            <a href="{{ url('view_information/'.$gully->id) }}" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-hover-border-green">View Information</a>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <br />
        @endcan
		<br />
	</div>
</body>
</html>