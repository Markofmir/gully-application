<!DOCTYPE html>
<html>
<head>
	<title>Map of Gullies</title>

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <h2 class="w3-green" style="text-shadow:1px 1px 0 #444">Locations</h2>
                <a href="/" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-green"><b>Back</b></a>
                <div class="map" id="app">
                    <gmap-map
                        :center="{lat:10, lng:10}"
                        :zoom="10"
                        map-type-id="terrain"
                        style="width: 100%; height:420px;"
                    ><!--add this to gmap-map to get click working
                    @click="handleMapClick"-->
                        <!--<gmap-info-window
                            :options="infoWindowOptions"
                            :position="infoWindowPosition"
                            :opened="infoWindowOpened"
                            
                        >Use thos to get map clicking working again @closeclick="handleInfoWindowClose"
                            <div class="info-window">
                                <h2 v-text="activeGully.name"></h2>
                                <h5 v-text="'Hours: ' + activeGully.hours"></h5>
                                <p v-text="activeGully.address"></p>
                                <p v-text="activeGully.city"></p>
                            </div>  this code is causing an error?
                        </gmap-info-window>-->
                        <gmap-marker
                            v-for="(r, index) in gullies"
                            :key="r.id"
                            :position="getPosition(r)"
                            :clickable="true"
                            :draggable="false"
                            @click="handleMarkerClicked(r)"
                        >
                        </gmap-marker>


                    </gmap-map>
                </div>
                <!--<div>
                    <h2>Add a Location</h2>
                    <form action="/" method="POST">
                        @csrf

                        <div>
                            <label for="name">Location Name:</label>
                            <br />
                            <input type="text" name="name" placeholder="Enter Name"/>
                        </div>
                        <br />
                        
                        <div>
                            <label for="address">Location Address:</label>
                            <br />
                            <input type="text" name="address" placeholder="Enter Address"/>
                        </div>
                        <br />

                        <div>
                            <label for="city">Location City:</label>
                            <br />
                            <input type="text" name="city" placeholder="Enter City"/>
                        </div>
                        <br />

                        <div>
                            <label for="hours">Location Opening Hours:</label>
                            <br />
                            <input type="text" name="hours" placeholder="Enter Hours"/>
                        </div>
                        <br />

                        <div>
                            <label for="latitude">Location Latitude:</label>
                            <br />
                            <input type="text" name="latitude" placeholder="Enter Latitude"/>
                        </div>
                        <br />

                        <div>
                            <label for="longitude">Location Longitude:</label>
                            <br />
                            <input type="text" name="longitude" placeholder="Enter Longitude"/>
                        </div>
                        <br />
                        <button type="submit">Add Location</button>
                    </form>
                </div>-->
                <!--<div>
                    <iframe width="700" height="400" src="https://www.opendatani.gov.uk/dataset/drainage-asset/resource/fa2a7de3-7e42-490a-a9d7-5f04f499c5d8/view/73bbd31b-b9de-4e29-abc8-86b1969800f7" frameBorder="0"></iframe>
                </div>-->
            </div> 
        </div>

        <script src="{{ mix('js/app.js') }}"></script>

    </body>
</html>