<!DOCTYPE html>
<html>
<head>
	<title>Council Gullies</title>

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
	<div>

		<h1 class="w3-green" style="text-shadow:1px 1px 0 #444">{{ $gullies[0]->SECTION_OFFICE_NAME }}</h1>

		<a href="/" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-green"><b>Back</b></a>

		<a href="/app" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-green"><b>Maps</b></a>


		<table class="w3-table w3-striped w3-border">
			<thead>
				<tr>
					<td><b>Section Name</b></td>
					<td><b>Gully Type Name</b></td>
					<td><b>Start Metres</b></td>
					<td><b>End Metres</b></td>
					<td><b>Location</b></td>				
				</tr>
			</thead>

			<tbody>
				@foreach($gullies as $gully)
				<tr>
					<td><a href="/gully/{{$gully->id}}">{{$gully->SECTION_NAME}}</a></td>
					<td>{{$gully->GULLY_TYPE_NAME}}</td>
					<td>{{$gully->START_METRES}}</td>
					<td>{{$gully->END_METRES}}</td>
					<td>{{$gully->LOCATION}}</td>				
				</tr>
				@endforeach
			</tbody>
		</table>
		<br />
	</div>
	<div class="w3-bar">
		{{ $gullies->links() }}
	</div>
</body>
</html>