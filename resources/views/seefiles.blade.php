<!DOCTYPE html>
<html>
<head>
	<title>User Gullies</title>

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
	<div>

		<h1 class="w3-green" style="text-shadow:1px 1px 0 #444">See Files</h1>

		<a href="/my_gullies" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-green"><b>Back</b></a>

		@foreach($images as $image)
			</br>
			<img src="{{asset('storage/media/'.$image->file_path)}}">
		@endforeach
	</div>
</body>
</html>