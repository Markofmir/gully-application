<!DOCTYPE html>
<html>
<head>
    <title>Gully Uploader</title>

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
    <div>
        <h1 class="w3-green" style="text-shadow:1px 1px 0 #444">Gully Uploader</h1>
        
        <div id="nav">
            <a href="/" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-green">Home</a>
            @can('send cleaning report')
            <a href="my_gullies" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-green">My Gullies</a>
            @endcan
            @role('admin')
            <a href="admin" class="w3-text-green w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-green">Admin</a>
            @endrole
        </div>
        
        <h2 style="text-shadow:1px 1px 0 #444">Councils</h2>
        <table class="w3-table w3-striped w3-border">
            <thead>
                <tr>
                    <td><b>Council Name</b></td>
                    <td><b>Show Gullies</b></td>
                </tr>
            </thead>
            <tbody>
                @foreach($councils as $council)
                <tr>
                    <td>{{$council->council_name}}</td>
                    <td>
                        <form  method="post">
                            @csrf
                            <a href="{{ url('gullies/'.$council->council_name) }}" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-hover-border-green">Show</a>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</body>
</html>