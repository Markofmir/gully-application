<!DOCTYPE html>
<html>
<head>
	<title>Council Gullies</title>

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
	<div>
		<a href="/" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-green"><b>Back</b></a>


		<table class="w3-table w3-striped w3-border">
			<thead>
				<tr>
					<td><b>Section Name</b></td>
					<td><b>Section Office Name</b></td>
					<td><b>Gully Type Name</b></td>
					<td><b>Start Metres</b></td>
					<td><b>End Metres</b></td>
					<td><b>Northing</b></td>
					<td><b>Easting</b></td>
					<td><b>Location</b></td>
				</tr>
			</thead>

			<tbody>
				@foreach($info as $gully)
				<tr>
					<td>{{$gully->SECTION_NAME}}</td>
					<td>{{$gully->SECTION_OFFICE_NAME}}</td>
					<td>{{$gully->GULLY_TYPE_NAME}}</td>
					<td>{{$gully->START_METRES}}</td>
					<td>{{$gully->END_METRES}}</td>
					<td>{{$gully->EASTING}}</td>
					<td>{{$gully->NORTHING}}</td>
					<td>{{$gully->LOCATION}}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		<br />

		<!--Add the ability to comment and stuff in here-->

		<div>
			<h1>Comments</h1>
			<a href="/gully/{{$gully->id}}/add-comment" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-green">Add Comment</a>
			<br />
			<br />
			<tbody>
				@foreach($comments as $comment)
					<tr><td>{{$comment->comment}}</td></tr>
					<br />
					<br />
				@endforeach
			</tbody>
		</div>

	</div>
</body>
</html>