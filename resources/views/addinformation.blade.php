<!DOCTYPE html>
<html>
<head>
	<title>Council Gullies</title>

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
    <h1 class="w3-green" style="text-shadow:1px 1px 0 #444">Add Gully Information</h1>

    <a href="/my_gullies" class=" w3-text-green w3-bar-item w3-button w3-hover-none w3-border-white w3-bottombar w3-hover-border-green"><b>Back</b></a>


    <form action="/add-information" method="post">
        @csrf
        <input type="text" name="gully_id" id="gully_id" placeholder="Gully ID">
        <br />
        <input type="text" name="information" id="information" placeholder="Information...">
        <br />
        <input type="submit" value="Add Gully Information">
    </form>
    

</body>
</html>