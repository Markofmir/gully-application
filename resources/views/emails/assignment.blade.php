<!DOCTYPE html>
<html>
<head>
    <title>You have been assigned to a gully</title>
</head>
<body>
    <div>
        <h1>You have been assigned to a gully</h1>
        <h2>Hello user {{ $assignment->assignee_id }}</h2>
        <h3>Info:</h3>
        <h4>{{ $assignment->gully_id }}</h4>
        <h4>{{ $assignment->assigner_id }}</h4>
        <h4>{{ $assignment->priority }}</h4>
        @if(!empty($note))
            <h4>{{ $note->notes }}</h4>
        @endif
    </div>
</body>
</html>